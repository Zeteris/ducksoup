using DuckSoup.Library;

namespace DuckSoup.Download
{
    public class DownloadServer : AsyncServer
    {
        public DownloadServer(SettingsManager settingsManager) : base(settingsManager)
        {
            PacketHandler = new PacketHandler(settingsManager.DownloadSettings.ClientWhitelist, settingsManager.DownloadSettings.ClientBlacklist);
        }
    }
}